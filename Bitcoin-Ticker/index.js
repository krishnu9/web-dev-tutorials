// jshint esversion: 6
// Module imports
const express = require("express");
const bodyParser = require("body-parser");
const request = require('request');

const app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));

// get request sends a html file to root route as response
app.get("/", function (req, res) {
    res.sendFile(__dirname + "/index.html");
});

// defining post request
app.post("/", function (req, res) {

    //console.log(req.body.crypto);

    var crypto = req.body.crypto;
    var fiat = req.body.fiat;

    var baseURL = "https://apiv2.bitcoinaverage.com/indices/global/ticker/";
    var finalURL = baseURL + crypto + fiat;

    // for creating http request and sending respones
    request(finalURL, function (error, response, body) {

        var data = JSON.parse(body); // to parse the JSON received
        var price = data.last;
        var currentDate = data.display_timestamp;

        res.write("<p>Date: " + currentDate + "</p>");
        res.write("<h1>The current price of " + crypto + " is " + price + " " + fiat + " </h1>");

        res.send();

        // res.send("<h1>The current price of " + crypto + " is " + price + " " + fiat + " </h1 > ");
    });

});
// app listens on this port
app.listen(3000, function () {
    console.log("Server running on port 3000.");
});