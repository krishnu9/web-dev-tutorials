// Add methods and properties to previously declared objects
function User(name) {
    this.name = name;
    this.life = 100;
    console.log(this.name + " joined the fight.");
    this.givelife = function givelife(targetUser) {
        targetUser.life += 1;
        console.log(this.name + " gave 1 life to " + targetUser.name);
    };
}

var Naruto = new User("Naruto");
var Sasuke = new User("Sasuke");

Naruto.givelife(Sasuke);
console.log("Naruto: " + Naruto.life);
console.log("Sasuke: " + Sasuke.life);

// You can add fucntions to all objects
User.prototype.attack = function atack(targetUser) {
    targetUser.life -= 5;
    this.life -= 1;
    console.log(this.name + " just attacked " + targetUser.name);
};

Naruto.attack(Sasuke);
console.log("Naruto: " + Naruto.life);
console.log("Sasuke: " + Sasuke.life);

// You can add properties to all objects
User.prototype.chakra = 60;
console.log("Naruto chakra: " + Naruto.chakra);
console.log("Sasuke chakra: " + Sasuke.chakra);