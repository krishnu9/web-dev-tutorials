// Include code in other .js file into this file
// module.exports.placeAnOrder = placeAnOrder; is written in source file

var Food = require('./multiple-requests');
Food.placeAnOrder(35);

// can also be done like this
module.exports = {
    printAvatar: function () {
        console.log("Avatar");
    },
    printMatrix: function () {
        console.log("Matrix");
    },
    favMovie: "Inception"
};