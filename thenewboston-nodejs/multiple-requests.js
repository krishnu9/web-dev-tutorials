function placeAnOrder(orderNumber) {
    console.log("Customer Order: ", orderNumber);

    cookAndDeliverFood(function () {
        console.log("Delivered food order: ", orderNumber);
    });

}

// Cook food for 5 seconds
function cookAndDeliverFood(callback) {
    setTimeout(callback, 5000);
}

// Place orders
placeAnOrder(1);
placeAnOrder(2);
placeAnOrder(3);
// placeAnOrder(4);
// placeAnOrder(5);
// placeAnOrder(6);

module.exports.placeAnOrder = placeAnOrder;