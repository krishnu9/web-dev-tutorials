var bucky = {
    favFood: "bacon",
    favMovie: "ironman"
};

// person ins just a reference to bucky. a new copy of obect is not created
var person = bucky;
person.favMovie = "captain-america";
console.log(bucky.favMovie);